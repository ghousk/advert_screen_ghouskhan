package com.innovativequest.advert_screen.model;

/**
 * Created by ghouskhan on 04/04/2017
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VehicleDetails implements Parcelable {

    @SerializedName("Make")
    @Expose
    private String Make;
    @SerializedName("Model")
    @Expose
    private String Model;
    @SerializedName("RegNumber")
    @Expose
    private String RegNumber;
    @SerializedName("OwnerId")
    @Expose
    private Integer OwnerId;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("Price")
    @Expose
    private String Price;
    @SerializedName("DatePosted")
    @Expose
    private String DatePosted;
    @SerializedName("FuelType")
    @Expose
    private String FuelType;

    @SerializedName("Transmission")
    @Expose
    private String Transmission;

    @SerializedName("YearMake")
    @Expose
    private String YearMake;

    @SerializedName("Colour")
    @Expose
    private String Colour;

    @SerializedName("EngineSize")
    @Expose
    private String EngineSize;

    @SerializedName("BodyType")
    @Expose
    private String BodyType;

    @SerializedName("SellerType")
    @Expose
    private String SellerType;

    @SerializedName("Mileage")
    @Expose
    private String Mileage;

    @SerializedName("Details")
    @Expose
    private String Details;

    @SerializedName("ImageUrl")
    @Expose
    private ArrayList<String> ImageUrl = new ArrayList<String>();

    /**
     *
     * @return
     * The Make
     */
    public String getMake() {
        return Make;
    }

    /**
     *
     * @return
     * The Model
     */
    public String getModel() {
        return Model;
    }


    /**
     *
     * @return
     * The RegNumber
     */
    public String getRegNumber() {
        return RegNumber;
    }

    /**
     *
     * @return
     * The OwnerId
     */
    public Integer getOwnerId() {
        return OwnerId;
    }

    /**
     *
     * @return
     * The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     *
     * @return
     * The Price
     */
    public String getPrice() {return Price; }

    /**
     *
     * @return
     * The DatePosted
     */
    public String getDatePosted() {
        return DatePosted;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getFuelType() {
        return FuelType;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getTransmission() {
        return Transmission;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getYearMake() {
        return YearMake;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getColour() {
        return Colour;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getEngineSize() {
        return EngineSize;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getBodyType() {
        return BodyType;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getSellerType() {
        return SellerType;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getMileage() {
        return Mileage;
    }

    /**
     *
     * @return
     * The FuelType
     */
    public String getDetails() {
        return Details;
    }

    /**
     *
     * @return
     * The ImageUrl
     */
    public ArrayList<String> getImageUrl() {
        return ImageUrl;
    }

    public static VehicleDetails fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, VehicleDetails.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Make);
        dest.writeString(this.Model);
        dest.writeString(this.RegNumber);
        dest.writeValue(this.OwnerId);
        dest.writeString(this.Title);
        dest.writeString(this.Price);
        dest.writeString(this.DatePosted);
        dest.writeString(this.FuelType);
        dest.writeString(this.Transmission);
        dest.writeString(this.YearMake);
        dest.writeString(this.Colour);
        dest.writeString(this.EngineSize);
        dest.writeString(this.BodyType);
        dest.writeString(this.SellerType);
        dest.writeString(this.Mileage);
        dest.writeString(this.Details);
        dest.writeStringList(this.ImageUrl);
    }

    public VehicleDetails() {
    }

    protected VehicleDetails(Parcel in) {
        this.Make = in.readString();
        this.Model = in.readString();
        this.RegNumber = in.readString();
        this.OwnerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Title = in.readString();
        this.Price = in.readString();
        this.DatePosted = in.readString();
        this.FuelType = in.readString();
        this.Transmission = in.readString();
        this.YearMake = in.readString();
        this.Colour = in.readString();
        this.EngineSize = in.readString();
        this.BodyType = in.readString();
        this.SellerType = in.readString();
        this.Mileage = in.readString();
        this.Details = in.readString();
        this.ImageUrl = in.createStringArrayList();
    }

    public static final Creator<VehicleDetails> CREATOR = new Creator<VehicleDetails>() {
        @Override
        public VehicleDetails createFromParcel(Parcel source) {
            return new VehicleDetails(source);
        }

        @Override
        public VehicleDetails[] newArray(int size) {
            return new VehicleDetails[size];
        }
    };
}