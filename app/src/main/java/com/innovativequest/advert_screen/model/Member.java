package com.innovativequest.advert_screen.model;

/**
 * Created by ghouskhan on 04/04/2017
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member implements Parcelable {

    @SerializedName("MobileNumber")
    @Expose
    private String MobileNumber;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Password")
    @Expose
    private String Password;
    @SerializedName("PostCode")
    @Expose
    private String PostCode;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("MemberSince")
    @Expose
    private String MemberSince;
    @SerializedName("AddressLine1")
    @Expose
    private String AddressLine1;
    @SerializedName("AddressLine2")
    @Expose
    private String AddressLine2;
    @SerializedName("AddressLine3")
    @Expose
    private String AddressLine3;
    @SerializedName("AddressCity")
    @Expose
    private String AddressCity;
    @SerializedName("AddressCounty")
    @Expose
    private String AddressCounty;

    /**
     *
     * @return
     * The MobileNumber
     */
    public String getMobileNumber() {
        return MobileNumber;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }


    /**
     *
     * @return
     * The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     *
     * @return
     * The LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @return
     * The PostCode
     */
    public String getPostCode() {
        return PostCode;
    }

    /**
     *
     * @return
     * The Title
     */
    public Object getTitle() {
        return Title;
    }

    /**
     *
     * @return
     * The MemberSince
     */
    public String getMemberSince() {return MemberSince; }

    /**
     *
     * @return
     * The AddressLine1
     */
    public String getAddressLine1() {
        return AddressLine1;
    }

    /**
     *
     * @return
     * The AddressLine2
     */
    public String getAddressLine2() {
        return AddressLine2;
    }

    /**
     *
     * @return
     * The AddressLine3
     */
    public String getAddressLine3() {
        return AddressLine3;
    }


    /**
     *
     * @return
     * The AddressCity
     */
    public String getAddressCity() {
        return AddressCity;
    }

    /**
     *
     * @return
     * The AddressCounty
     */
    public String getAddressCounty() {
        return AddressCounty;
    }

    public static Member fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Member.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.MobileNumber);
        dest.writeString(this.Email);
        dest.writeString(this.FirstName);
        dest.writeValue(this.Id);
        dest.writeString(this.LastName);
        dest.writeString(this.Password);
        dest.writeString(this.PostCode);
        dest.writeString(this.Title);
        dest.writeString(this.MemberSince);
        dest.writeString(this.AddressLine1);
        dest.writeString(this.AddressLine2);
        dest.writeString(this.AddressLine3);
        dest.writeString(this.AddressCity);
        dest.writeString(this.AddressCounty);
    }

    public Member() {
    }

    protected Member(Parcel in) {
        this.MobileNumber = in.readString();
        this.Email = in.readString();
        this.FirstName = in.readString();
        this.Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.LastName = in.readString();
        this.Password = in.readString();
        this.PostCode = in.readString();
        this.Title = in.readString();
        this.MemberSince = in.readString();
        this.AddressLine1 = in.readString();
        this.AddressLine2 = in.readString();
        this.AddressLine3 = in.readString();
        this.AddressCity = in.readString();
        this.AddressCounty = in.readString();
    }

    public static final Creator<Member> CREATOR = new Creator<Member>() {
        @Override
        public Member createFromParcel(Parcel source) {
            return new Member(source);
        }

        @Override
        public Member[] newArray(int size) {
            return new Member[size];
        }
    };
}