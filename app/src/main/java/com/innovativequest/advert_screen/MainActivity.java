package com.innovativequest.advert_screen;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.innovativequest.advert_screen.dataaccess.VehiclesDataFactory;
import com.innovativequest.advert_screen.fragments.MainActivityFragment;
import com.innovativequest.advert_screen.model.Member;
import com.innovativequest.advert_screen.model.VehicleDetails;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private Member mMember;
    private VehicleDetails mVehicleDetails;
    final static String MEMBER_TAG = "member_tag";
    final static String VEHICLE_TAG = "vehicle_tag";

    // Toolbar bindings
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            loadData();
        }
        else if(mVehicleDetails != null && mMember != null){
            initRootFragment();
        }
    }

    private void initRootFragment() {
        Fragment rootFragment = new MainActivityFragment().newInstanceWithMode(mMember, mVehicleDetails);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_container, rootFragment)
                .commit();
    }

    private void loadData() {
        final VehiclesDataFactory dataFactory = new VehiclesDataFactory(this);
        dataFactory.getMember( new VehiclesDataFactory.MemberDetailsDataReceivedCallback(){

            @Override
            public void OnMemberDetailsDataReceived(Member response) {
                mMember = response;
                dataFactory.getVehicleDetails(new VehiclesDataFactory.VehicleDetailsDataReceivedCallback() {
                    @Override
                    public void OnVehicleDetailsDataReceived(VehicleDetails response) {
                        mVehicleDetails = response;
                        initRootFragment();
                    }
                });
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putParcelable(MEMBER_TAG, mMember);
        outState.putParcelable(VEHICLE_TAG, mVehicleDetails);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);

        if (savedInstanceState != null) {
            mMember = savedInstanceState.getParcelable(MEMBER_TAG);
            mVehicleDetails = savedInstanceState.getParcelable(VEHICLE_TAG);
        }
    }
}
