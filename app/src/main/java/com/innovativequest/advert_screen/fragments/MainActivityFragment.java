package com.innovativequest.advert_screen.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.innovativequest.advert_screen.Adapters.CustomPagerAdapter;
import com.innovativequest.advert_screen.R;
import com.innovativequest.advert_screen.model.Member;
import com.innovativequest.advert_screen.model.VehicleDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    final static String MEMBER_TAG = "member_tag";
    final static String VEHICLE_TAG = "vehicle_tag";
    static final int MIN_MAIN_IMAGE_HEIGHT_DP = 200;
    private int mOverallYScroll = 0;
    private View mRootView;
    private VehicleDetails mVehicleDetails;
    private Member mMember;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    @Bind(R.id.view_pager)
    AutoScrollViewPager mPager;
    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    PagerAdapter mPagerAdapter;

    @Bind(R.id.scrollView_layout)
    View mScollView;

    @Bind(R.id.imageOverlayView)
    View mImageOverlayView;

    @Bind(R.id.titleTV)
    TextView mTitleTv;

    @Bind(R.id.addressTv)
    TextView mAddressTv;

    @Bind(R.id.priceTv)
    TextView mPriceTv;

    @Bind(R.id.date_posted_tv)
    TextView mDate_posted_tv;

    @Bind(R.id.fuel_type_tv)
    TextView mFuel_type_tv;

    @Bind(R.id.transmission_type_tv)
    TextView mTransmission_type_tv;

    @Bind(R.id.year_make_tv)
    TextView mYear_make_tv;

    @Bind(R.id.colour_tv)
    TextView mColour_tv;

    @Bind(R.id.engine_size_tv)
    TextView mEngine_size_tv;

    @Bind(R.id.body_type_tv)
    TextView mBody_type_tv;

    @Bind(R.id.seller_type_tv)
    TextView mSeller_type_tv;

    @Bind(R.id.mileage_tv)
    TextView mMileage_tv;

    @Bind(R.id.details_tv)
    TextView mDetails_tv;

    @Bind(R.id.contact_seller_tv)
    TextView mContact_seller_tv;

    @Bind(R.id.posting_length_tv)
    TextView mPosting_length_tv;

    private boolean imageOverlayFadedIn = false;

    public static MainActivityFragment newInstanceWithMode(Member member, VehicleDetails vehicleDetails) {
        MainActivityFragment fragment = new MainActivityFragment();
        Bundle args = new Bundle();
        args.putParcelable(MEMBER_TAG, member);
        args.putParcelable(VEHICLE_TAG, vehicleDetails);
        fragment.setArguments(args);
        return fragment;
    }
    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(MainActivityFragment.this, mRootView);

        InitUi();

        return mRootView;
    }


    private void InitUi(){
        Member member = null;
        VehicleDetails vehicleDetails = null;
        if (getArguments().containsKey(MEMBER_TAG)) {
            mMember = getArguments().getParcelable(MEMBER_TAG);
        }
        if (getArguments().containsKey(VEHICLE_TAG)) {
            mVehicleDetails = getArguments().getParcelable(VEHICLE_TAG);
        }

        initialisePager();
        initDetails();

//        final FrameLayout mainImageContainer = (FrameLayout)mRootView.findViewById(R.id.mainImageContainer);
//        final float mainImageContainerHeight = (int)getResources().getDimension(R.dimen.main_image_height);
//        final int minMainImageHeight = (int)dpToPixel(MIN_MAIN_IMAGE_HEIGHT_DP);
//        mScollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                mOverallYScroll = oldScrollY - scrollY;
//                ViewGroup.LayoutParams p = mainImageContainer.getLayoutParams();
//                p.height = (int)(mainImageContainerHeight - mOverallYScroll);
//
//                // set min height
//                if (mainImageContainerHeight - mOverallYScroll < minMainImageHeight) {
//                    fadeInImageOverlay();
//                    p.height = minMainImageHeight;
//                } else {
//                    fadeOutImageOverlay();
//                }
//
//                // set max height
//                if (p.height > mainImageContainerHeight) {
//                    p.height = (int)mainImageContainerHeight;
//                }
//
//                mainImageContainer.requestLayout();
//            }
//        });
    }

    private void initialisePager(){
        CustomPagerAdapter adapter = new CustomPagerAdapter(getActivity().getSupportFragmentManager(), mVehicleDetails.getImageUrl());
        if(getActivity().getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE){
            adapter.setPageWidth(0.4f);
            mPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        }
        mPager.setInterval(5000);
        mPager.setScrollDurationFactor(10);
        mPager.setCurrentItem(0);
        mPager.startAutoScroll();
        mPager.setAdapter(adapter);
    }

    private void initDetails(){
        try {
            mTitleTv.setText(mVehicleDetails.getTitle());
            mAddressTv.setText(mMember.getAddressLine1() + " , " +
                    mMember.getAddressLine2() + " , " +
                    mMember.getAddressCity());
            mPriceTv.setText(mVehicleDetails.getPrice());
            mDate_posted_tv.setText(mVehicleDetails.getDatePosted());
            mFuel_type_tv.setText(mVehicleDetails.getFuelType());
            mTransmission_type_tv.setText(mVehicleDetails.getTransmission());
            mYear_make_tv.setText(mVehicleDetails.getYearMake());
            mColour_tv.setText(mVehicleDetails.getColour());
            mEngine_size_tv.setText(mVehicleDetails.getEngineSize());
            mBody_type_tv.setText(mVehicleDetails.getBodyType());
            mSeller_type_tv.setText(mVehicleDetails.getSellerType());
            mMileage_tv.setText(mVehicleDetails.getMileage());
            mDetails_tv.setText(mVehicleDetails.getDetails());

            mContact_seller_tv.setText( getString( R.string.contact_seller, mMember.getFirstName() ) );
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String dateStr = mMember.getMemberSince();
            Date date = null;

            if (dateStr != null) {
                try {
                    date = inputFormat.parse(dateStr);
                    dateStr = inputFormat.format(date); // Re-use dateStr

                    Log.i("mini", "Converted Date Today:" + dateStr);
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            mPosting_length_tv.setText(getString(R.string.posting_length, "5")+getString(R.string.years));
        }
        catch (NullPointerException e){
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private float dpToPixel(int dp) {
        Resources r = this.getActivity().getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    private void fadeInImageOverlay() {

        if (!imageOverlayFadedIn) {
            imageOverlayFadedIn = true;
            mImageOverlayView.setVisibility(View.VISIBLE);

            AlphaAnimation animation1 = new AlphaAnimation(0.0f, 0.8f);
            animation1.setDuration(300);
            animation1.setFillAfter(true);
            mImageOverlayView.startAnimation(animation1);
        }
    }

    private void fadeOutImageOverlay() {

        if (imageOverlayFadedIn) {
            imageOverlayFadedIn = false;
            AlphaAnimation animation1 = new AlphaAnimation(0.8f, 0.0f);
            animation1.setDuration(300);
            animation1.setFillAfter(true);
            animation1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    mImageOverlayView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            mImageOverlayView.startAnimation(animation1);
        }
    }
}
