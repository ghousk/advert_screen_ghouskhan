package com.innovativequest.advert_screen.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.innovativequest.advert_screen.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ghouskhan on 05/04/2017.
 */
public class SlidePageFragment extends Fragment {
    private int mPageNumber;
    private ArrayList<String> mContentList;
    public static final String ARG_PAGE = "page";
    private static final String ARG_LIST_KEY = "list_key";

    public static SlidePageFragment getInstance(int pageNumber, ArrayList<String> list) {
        SlidePageFragment fragment = new SlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        args.putSerializable(ARG_LIST_KEY, list);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        mContentList = (ArrayList<String> ) getArguments().getSerializable(ARG_LIST_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = (View) inflater.inflate(R.layout.landing_carrossel_item, container, false);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.itemIV);

        Picasso.with(getContext()).load(mContentList.get(mPageNumber)).into(imageView, new ImageLoadedCallback(){
            @Override
            public void onSuccess() {

            }
        });
        return rootView;
    }

    private class ImageLoadedCallback implements Callback {

        public  ImageLoadedCallback(){

        }

        @Override
        public void onSuccess() {

        }

        @Override
        public void onError() {

        }
    }
}
