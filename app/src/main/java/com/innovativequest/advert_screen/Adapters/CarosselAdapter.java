package com.innovativequest.advert_screen.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.innovativequest.advert_screen.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

//public class CarosselAdapter extends PagerAdapter
//{
//    List<String> imageArray = new ArrayList<String>();
//    private Context mContext;
//
//    public CarosselAdapter(Context act, List<String> imgArra)
//    {
//        if (imageArray != null && imgArra != null) {
//            imageArray.addAll(imgArra);
//        }
//        mContext = act;
//    }
//
//    public int getCount()
//    {
//        return imageArray.size();
//    }
//
//    public Object instantiateItem(View collection, int position)
//    {
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        View pageView = inflater.inflate(R.layout.landing_carrossel_item, null);
//        ImageView imageView = (ImageView) pageView.findViewById(R.id.itemIV);
//        ProgressBar ivProgressBar = (ProgressBar) pageView.findViewById(R.id.progressBar);
//        Picasso.with(mContext).load(
//                                (((imageArray.get(position) != null) &&
//                                        (imageArray.get(position).length() > 2)) ?
//                                        imageArray.get(position) : "")).into(imageView, new ImageLoadedCallback(ivProgressBar){
//            @Override
//            public void onSuccess() {
//                if (this.progressBar != null) {
//                    this.progressBar.setVisibility(View.GONE);
//                }
//            }
//        });
//
//
//        ((ViewPager) collection).addView(pageView, 0);
//
//        return pageView;
//    }
//
//    @Override
//    public void destroyItem(View arg0, int arg1, Object arg2)
//    {
//        ((ViewPager) arg0).removeView((View) arg2);
//    }
//
//    @Override
//    public boolean isViewFromObject(View arg0, Object arg1)
//    {
//        return arg0 == ((View) arg1);
//    }
//
//    @Override
//    public Parcelable saveState()
//    {
//        return null;
//    }
//
//    private class ImageLoadedCallback implements Callback {
//        ProgressBar progressBar;
//
//        public  ImageLoadedCallback(ProgressBar progBar){
//            progressBar = progBar;
//        }
//
//        @Override
//        public void onSuccess() {
//
//        }
//
//        @Override
//        public void onError() {
//
//        }
//    }
//
//}