package com.innovativequest.advert_screen.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.innovativequest.advert_screen.fragments.SlidePageFragment;

import java.util.ArrayList;

/**
 * Created by ghouskhan on 05/04/2017.
 */

public class CustomPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> images;
    private float pageWidth = 1f;

    public CustomPagerAdapter(FragmentManager fm, ArrayList<String> imagesList) {
        super(fm);
        this.images = imagesList;
    }

    @Override
    public Fragment getItem(int position) {
        return SlidePageFragment.getInstance(position, images);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    public void setPageWidth(float width){
        pageWidth = width;
    }
    @Override
    public float getPageWidth(int position) {
        return(pageWidth);
    }
}
