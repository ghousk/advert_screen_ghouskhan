package com.innovativequest.advert_screen.dataaccess;

import android.content.Context;

import com.innovativequest.advert_screen.model.Member;
import com.innovativequest.advert_screen.model.VehicleDetails;
import com.innovativequest.advert_screen.utils.AssetUtil;

/**
 * Added by Ghous Khan on 04/04/2017
 */
public class VehiclesDataFactory {

    private Context mContext;

    public VehiclesDataFactory(Context context) {
        mContext = context;
    }

    public void getMember(MemberDetailsDataReceivedCallback callback) {

        String json = AssetUtil.getJsonFromAssetPath("fakedata/member.json", mContext);
        callback.OnMemberDetailsDataReceived(Member.fromJson(json));
    }

    public interface MemberDetailsDataReceivedCallback {
        public void OnMemberDetailsDataReceived(Member response);
    }

    public void getVehicleDetails(VehicleDetailsDataReceivedCallback callback) {

        String json = AssetUtil.getJsonFromAssetPath("fakedata/vehicleDetails.json", mContext);
        callback.OnVehicleDetailsDataReceived(VehicleDetails.fromJson(json));
    }

    public interface VehicleDetailsDataReceivedCallback {
        public void OnVehicleDetailsDataReceived(VehicleDetails response);
    }
}
