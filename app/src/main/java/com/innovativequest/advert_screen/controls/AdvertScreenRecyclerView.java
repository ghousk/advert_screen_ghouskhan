package com.innovativequest.advert_screen.controls;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by ghouskhan on 04/04/17.
 */
public class AdvertScreenRecyclerView extends RecyclerView {

    public AdvertScreenRecyclerView(Context context) {
        super(context);
    }

    public AdvertScreenRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdvertScreenRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public int getHorizontalOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int getVerticalOffset() {
        return super.computeVerticalScrollOffset();
    }
}
