package com.innovativequest.advert_screen.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 */
public class AssetUtil {

    public static String getJsonFromAssetPath(String path, Context context) {
        try {
            InputStream jsonStream = context.getAssets().open(path);
            BufferedReader r = new BufferedReader(new InputStreamReader(jsonStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            return total.toString();
        }
        catch(IOException ex) {

            Log.e("HomeCollectionAdapter", ex.getMessage());
            return null;
        }
    }

    public static Drawable getBitmapFromAssetPath(String path, Context context) {
        try {
            InputStream imageStream = context.getAssets().open(path);
            Drawable drawable = Drawable.createFromStream(imageStream, null);
            return drawable;
        }
        catch(IOException ex) {

            Log.e("HomeCollectionAdapter", ex.getMessage());
            return null;
        }
    }
}
